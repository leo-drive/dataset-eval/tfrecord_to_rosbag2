from visualization_msgs.msg import MarkerArray
from visualization_msgs.msg import Marker
from builtin_interfaces.msg import Time

class MarkerDeleter(object):
    def __init__(self):
        self.current_frame_id = 0
        self.current_sec = 0
        self.current_nsec = 0

        self.list_cube_text_arrow_ids_previous_frame = []
        self.list_cube_text_arrow_ids_current_frame = []

    def updateFrame(self, frame_id, sec, nsec):
        self.current_frame_id = frame_id
        self.current_sec = sec
        self.current_nsec = nsec

    def updateState(self):
        self.list_cube_text_arrow_ids_previous_frame = \
            self.list_cube_text_arrow_ids_current_frame

    def take_Cube_Text_Arrow_Ids(self, id):
        self.list_cube_text_arrow_ids_current_frame = []
        self.list_cube_text_arrow_ids_current_frame.append(id)

    def giveDeletedMarkersCubeTextArrow(self):
        time_stamp = Time()
        time_stamp.sec = self.current_sec
        time_stamp.nanosec = self.current_nsec

        list_markers_to_be_delete = []

        for id in self.list_cube_text_arrow_ids_previous_frame:
            cube_marker = Marker()
            cube_marker.ns = "marker_cube"
            cube_marker.header.frame_id = "vehicle"
            cube_marker.header.stamp = time_stamp
            cube_marker.id = id
            cube_marker.type = Marker.CUBE
            cube_marker.action = Marker.DELETE

            arrow_marker = Marker()
            arrow_marker.ns = "marker_arrow"
            arrow_marker.header.frame_id = "vehicle"
            arrow_marker.header.stamp = time_stamp
            arrow_marker.id = id
            arrow_marker.type = Marker.ARROW
            arrow_marker.action = Marker.DELETE

            text_marker = Marker()
            text_marker.ns = "marker_text"
            text_marker.header.frame_id = "vehicle"
            text_marker.header.stamp = time_stamp
            text_marker.id = id
            text_marker.type = Marker.TEXT_VIEW_FACING
            text_marker.action = Marker.DELETE

            list_markers_to_be_delete.append(cube_marker)
            list_markers_to_be_delete.append(arrow_marker)
            list_markers_to_be_delete.append(text_marker)

        return list_markers_to_be_delete






