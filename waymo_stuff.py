import tensorflow.compat.v1 as tf
import math
import itertools
# tf.enable_eager_execution()
from waymo_open_dataset.utils import range_image_utils
from waymo_open_dataset.utils import transform_utils
from waymo_open_dataset.utils import frame_utils
from waymo_open_dataset import dataset_pb2 as open_dataset
import numpy as np


class WaymoStuff(object):
    def __init__(self):
        pass

    def convert_range_image_to_point_cloud(self, frame, range_images, camera_projections, range_image_top_pose,
                                           ri_index=0):
        calibrations = sorted(frame.context.laser_calibrations, key=lambda c: c.name)
        lasers = sorted(frame.lasers, key=lambda laser: laser.name)
        points = []
        cp_points = []
        frame_pose = tf.convert_to_tensor(np.reshape(np.array(frame.pose.transform), [4, 4]))
        # [H, W, 6]
        range_image_top_pose_tensor = tf.reshape(tf.convert_to_tensor(range_image_top_pose.data),
                                                 range_image_top_pose.shape.dims)
        # [H, W, 3, 3]
        range_image_top_pose_tensor_rotation = transform_utils.get_rotation_matrix(range_image_top_pose_tensor[..., 0],
                                                                                   range_image_top_pose_tensor[..., 1],
                                                                                   range_image_top_pose_tensor[..., 2])
        range_image_top_pose_tensor_translation = range_image_top_pose_tensor[..., 3:]
        range_image_top_pose_tensor = transform_utils.get_transform(range_image_top_pose_tensor_rotation,
                                                                    range_image_top_pose_tensor_translation)
        for c in calibrations:
            range_image = range_images[c.name][ri_index]
            if len(c.beam_inclinations) == 0:
                beam_inclinations = range_image_utils.compute_inclination(
                    tf.constant([c.beam_inclination_min, c.beam_inclination_max]), height=range_image.shape.dims[0])
            else:
                beam_inclinations = tf.constant(c.beam_inclinations)
            beam_inclinations = tf.reverse(beam_inclinations, axis=[-1])
            extrinsic = np.reshape(np.array(c.extrinsic.transform), [4, 4])
            range_image_tensor = tf.reshape(tf.convert_to_tensor(range_image.data), range_image.shape.dims)
            pixel_pose_local = None
            frame_pose_local = None
            if c.name == open_dataset.LaserName.TOP:
                pixel_pose_local = range_image_top_pose_tensor
                pixel_pose_local = tf.expand_dims(pixel_pose_local, axis=0)
                frame_pose_local = tf.expand_dims(frame_pose, axis=0)
            range_image_mask = range_image_tensor[..., 0] > 0
            range_image_cartesian = range_image_utils.extract_point_cloud_from_range_image(
                tf.expand_dims(range_image_tensor[..., 0], axis=0), tf.expand_dims(extrinsic, axis=0),
                tf.expand_dims(tf.convert_to_tensor(beam_inclinations), axis=0), pixel_pose=pixel_pose_local,
                frame_pose=frame_pose_local)
            range_image_cartesian = tf.squeeze(range_image_cartesian, axis=0)
            points_tensor = tf.gather_nd(range_image_cartesian, tf.where(range_image_mask))
            cp = camera_projections[c.name][0]
            cp_tensor = tf.reshape(tf.convert_to_tensor(cp.data), cp.shape.dims)
            cp_points_tensor = tf.gather_nd(cp_tensor, tf.where(range_image_mask))
            points.append(points_tensor.numpy())
            cp_points.append(cp_points_tensor.numpy())
        return points, cp_points

    def parse_range_image_and_camera_projection(self, frame):
        range_images = {}
        camera_projections = {}
        range_image_top_pose = None
        for laser in frame.lasers:
            if len(laser.ri_return1.range_image_compressed) > 0:
                range_image_str_tensor = tf.io.decode_compressed(laser.ri_return1.range_image_compressed, 'ZLIB')
                ri = open_dataset.MatrixFloat()
                ri.ParseFromString(bytearray(range_image_str_tensor.numpy()))
                range_images[laser.name] = [ri]
                if laser.name == open_dataset.LaserName.TOP:
                    range_image_top_pose_str_tensor = tf.io.decode_compressed(
                        laser.ri_return1.range_image_pose_compressed, 'ZLIB')
                    range_image_top_pose = open_dataset.MatrixFloat()
                    range_image_top_pose.ParseFromString(bytearray(range_image_top_pose_str_tensor.numpy()))
                camera_projection_str_tensor = tf.io.decode_compressed(laser.ri_return1.camera_projection_compressed,
                                                                       'ZLIB')
                cp = open_dataset.MatrixInt32()
                cp.ParseFromString(bytearray(camera_projection_str_tensor.numpy()))
                camera_projections[laser.name] = [cp]
            if len(laser.ri_return2.range_image_compressed) > 0:
                range_image_str_tensor = tf.io.decode_compressed(laser.ri_return2.range_image_compressed, 'ZLIB')
                ri = open_dataset.MatrixFloat()
                ri.ParseFromString(bytearray(range_image_str_tensor.numpy()))
                range_images[laser.name].append(ri)
                camera_projection_str_tensor = tf.io.decode_compressed(laser.ri_return2.camera_projection_compressed,
                                                                       'ZLIB')
                cp = open_dataset.MatrixInt32()
                cp.ParseFromString(bytearray(camera_projection_str_tensor.numpy()))
                camera_projections[laser.name].append(cp)
        return range_images, camera_projections, range_image_top_pose


    def find_camera_calibration(self, name, camera_calibrations):
        for calibrations in camera_calibrations:
            if calibrations.name == name:
                calibration = calibrations
                break
        return calibration

    def isRotationMatrix(self, R):
        Rt = np.transpose(R)
        shouldBeIdentity = np.dot(Rt, R)
        I = np.identity(3, dtype=R.dtype)
        n = np.linalg.norm(I - shouldBeIdentity)
        return n < 1e-6

    def rotationMatrixToEulerAngles(self, R):

        assert (self.isRotationMatrix(R))

        sy = math.sqrt(R[0, 0] * R[0, 0] + R[1, 0] * R[1, 0])

        singular = sy < 1e-6

        if not singular:
            x = math.atan2(R[2, 1], R[2, 2])
            y = math.atan2(-R[2, 0], sy)
            z = math.atan2(R[1, 0], R[0, 0])
        else:
            x = math.atan2(-R[1, 2], R[1, 1])
            y = math.atan2(-R[2, 0], sy)
            z = 0

        return np.array([x, y, z])

    def find_lidar_calibration(self,name, laser_calibrations):
        for calibrations in laser_calibrations:
            if calibrations.name == name:
                calibration = calibrations
                break
        return calibration
