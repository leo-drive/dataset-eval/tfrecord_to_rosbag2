from os import listdir
from os.path import isfile, join
import numpy as np
import os
import tensorflow.compat.v1 as tf
import math
import itertools
import codecs
# tf.enable_eager_execution()
from waymo_open_dataset.utils import range_image_utils
from waymo_open_dataset.utils import transform_utils
from waymo_open_dataset.utils import frame_utils
from waymo_open_dataset import dataset_pb2 as open_dataset

from sensor_msgs.msg import PointCloud2, PointField, Image, CameraInfo
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import TransformStamped
from tf2_msgs.msg import TFMessage
import geometry_msgs.msg

# Alternative for tf py
from squaternion import Quaternion

import rclpy
from rclpy.duration import Duration
import builtin_interfaces.msg
from std_msgs.msg import Header
from waymo_stuff import WaymoStuff
import sensor_msgs.msg as sensor_msgs
import std_msgs.msg as std_msgs

# for ground truth objects:
from autoware_auto_msgs.msg import DetectedObjects
from autoware_auto_msgs.msg import DetectedObject
from autoware_auto_msgs.msg import DetectedObjectKinematics
from autoware_auto_msgs.msg import ObjectClassification
from autoware_auto_msgs.msg import Shape
from geometry_msgs.msg import Polygon
from geometry_msgs.msg import Point32
from geometry_msgs.msg import PoseWithCovariance

from nav_msgs.msg import Odometry

from visualization_msgs.msg import MarkerArray
from visualization_msgs.msg import Marker

from polygon_point_generator import PolygonPointGenerator
from marker_deleter import MarkerDeleter

from scipy.spatial import ConvexHull, convex_hull_plot_2d


class Helper(object):
    def __init__(self, path_tfrecord):
        self.path_tfrecord = path_tfrecord
        self.segment_name = self.path_tfrecord.split('/')[-1].split('-')[-1].split('.')[0].split('_with')[0]
        self.marker_deleter = MarkerDeleter()
        print("SEGMENT NAME:", self.segment_name)

        self.waymoStuff = WaymoStuff()

        self.list_frame_by_frame_lidar_data = []
        self.list_frame_by_frame_camera_data = []
        self.list_frame_by_frame_lidar_labels = []
        self.list_frame_by_frame_detected_objects = []
        self.list_frame_by_frame_trans = []
        self.list_frame_by_frame_camera_info = []
        self.list_frame_by_frame_odometry = []
        self.list_timestamp = []

        self.trans_t_zero_inverse = np.array([0])

    def readDataset(self):
        dataset = tf.data.TFRecordDataset(self.path_tfrecord, compression_type='')
        list_frame_ids = []
        for frame_id, data in enumerate(dataset):
            transforms = TFMessage()
            list_frame_ids.append(frame_id)
            frame = open_dataset.Frame()
            frame.ParseFromString(bytearray(data.numpy()))

            # print("Segment: ", frame.context.name)
            print("Frame id: ", frame_id)
            frame_stamp_secs = int(frame.timestamp_micros / 1000000)
            frame_stamp_nsecs = int((frame.timestamp_micros / 1000000.0 - frame_stamp_secs) * 1000000000)
            self.marker_deleter.updateFrame(frame_id, frame_stamp_secs,
                                            frame_stamp_nsecs)

            (range_images, camera_projections,
             range_image_top_pose) = self.waymoStuff.parse_range_image_and_camera_projection(
                frame)
            points, cp_points = self.waymoStuff.convert_range_image_to_point_cloud(frame, range_images,
                                                                                   camera_projections,
                                                                                   range_image_top_pose)

            # Collect lidar data in a single frame:
            lidar_data_all_single_frame = []
            for index, lidar_points in enumerate(points):
                lidar_name = index + 1
                name_lidar_topic = ''
                name_frame_id = ''
                if lidar_name == 1:
                    name_lidar_topic = '/cloud_top'
                    name_frame_id = 'lid_top'
                elif lidar_name == 2:
                    name_lidar_topic = '/cloud_front'
                    name_frame_id = 'lid_front'
                elif lidar_name == 3:
                    name_lidar_topic = '/cloud_left'
                    name_frame_id = 'lid_left'
                elif lidar_name == 4:
                    name_lidar_topic = '/cloud_right'
                    name_frame_id = 'lid_right'
                elif lidar_name == 5:
                    name_lidar_topic = '/cloud_rear'
                    name_frame_id = 'lid_rear'

                points_single_lidar = []

                for point in points[index]:
                    pt = [point[0], point[1], point[2]]
                    points_single_lidar.append(pt)

                msg_point_cloud = self.point_cloud(np.array(points_single_lidar), "vehicle")
                timestamp = builtin_interfaces.msg.Time()
                timestamp.sec = frame_stamp_secs
                timestamp.nanosec = frame_stamp_nsecs
                msg_point_cloud.header.stamp = timestamp
                timestamp_bag_nano = int(frame_stamp_secs * 1e9) + int(frame_stamp_nsecs)
                lidar_data_all_single_frame.append([msg_point_cloud, name_lidar_topic, timestamp_bag_nano])

                # Collect lid pose in vehicle frame:
                calibration = self.waymoStuff.find_lidar_calibration(lidar_name, frame.context.laser_calibrations)
                calibration_array = np.array([(calibration.extrinsic.transform[0], calibration.extrinsic.transform[1],
                                               calibration.extrinsic.transform[2], calibration.extrinsic.transform[3]),
                                              (calibration.extrinsic.transform[4], calibration.extrinsic.transform[5],
                                               calibration.extrinsic.transform[6], calibration.extrinsic.transform[7]),
                                              (calibration.extrinsic.transform[8], calibration.extrinsic.transform[9],
                                               calibration.extrinsic.transform[10],
                                               calibration.extrinsic.transform[11]),
                                              (calibration.extrinsic.transform[12], calibration.extrinsic.transform[13],
                                               calibration.extrinsic.transform[14],
                                               calibration.extrinsic.transform[15])])
                rotation_matrix = np.array([(calibration_array[0][0], calibration_array[0][1], calibration_array[0][2]),
                                            (calibration_array[1][0], calibration_array[1][1], calibration_array[1][2]),
                                            (
                                                calibration_array[2][0], calibration_array[2][1],
                                                calibration_array[2][2])])

                r, p, y = self.waymoStuff.rotationMatrixToEulerAngles(rotation_matrix)
                q = Quaternion.from_euler(r, p, y, degrees=False)
                quaternion = q.normalize

                transform = TransformStamped()
                transform.header.frame_id = 'vehicle'
                transform.child_frame_id = name_frame_id
                timestamp = builtin_interfaces.msg.Time()
                timestamp.sec = frame_stamp_secs
                timestamp.nanosec = frame_stamp_nsecs
                transform.header.stamp = timestamp

                transform.transform.rotation.x = quaternion.x
                transform.transform.rotation.y = quaternion.y
                transform.transform.rotation.z = quaternion.z
                transform.transform.rotation.w = quaternion.w
                transform.transform.translation.x = calibration_array[0][3]
                transform.transform.translation.y = calibration_array[1][3]
                transform.transform.translation.z = calibration_array[2][3]
                transforms.transforms.append(transform)
            self.list_frame_by_frame_lidar_data.append(lidar_data_all_single_frame)

            # Collect camera data in a single frame:
            camera_data_all_single_frame = []
            for image in frame.images:
                msg_image = Image()
                msg_image.header.frame_id = "vehicle"
                timestamp = builtin_interfaces.msg.Time()
                timestamp.sec = frame_stamp_secs
                timestamp.nanosec = frame_stamp_nsecs
                msg_image.header.stamp = timestamp

                name_camera_topic = ''

                if image.name == 1:
                    name_camera_topic = '/camera_front'
                if image.name == 2:
                    name_camera_topic = '/camera_frontleft'
                if image.name == 3:
                    name_camera_topic = '/camera_frontright'
                if image.name == 4:
                    name_camera_topic = '/camera_sideleft'
                if image.name == 5:
                    name_camera_topic = '/camera_sideright'

                calibration = self.waymoStuff.find_camera_calibration(image.name, frame.context.camera_calibrations)
                msg_image.height = calibration.height
                msg_image.width = calibration.width
                msg_image.encoding = "rgb8"
                msg_image.data = np.array(tf.image.decode_jpeg(image.image)).tostring()
                timestamp_bag_nano = int(frame_stamp_secs * 1e9) + int(frame_stamp_nsecs)
                camera_data_all_single_frame.append([msg_image, name_camera_topic, timestamp_bag_nano])

            self.list_frame_by_frame_camera_data.append(camera_data_all_single_frame)

            # Collect lidar labels:
            lidar_label_markerArr = MarkerArray()
            for index, laser_label in enumerate(frame.laser_labels):
                ID = codecs.encode(laser_label.id[len(laser_label.id) - 8:len(laser_label.id)])
                ID = int.from_bytes(ID, byteorder='big', signed=False)
                ID = int(str(ID)[:5])
                self.marker_deleter.take_Cube_Text_Arrow_Ids(ID)

                marker_cube = self.giveCubeLabel(frame_stamp_secs, frame_stamp_nsecs, ID, laser_label, frame_id)
                marker_text_id = self.giveIdLabelText(frame_stamp_secs, frame_stamp_nsecs, ID, laser_label, frame_id)
                marker_arrow_heading = self.giveArrowMarker(frame_stamp_secs, frame_stamp_nsecs, laser_label, ID,
                                                            frame_id)

                lidar_label_markerArr.markers.append(marker_cube)
                lidar_label_markerArr.markers.append(marker_text_id)
                lidar_label_markerArr.markers.append(marker_arrow_heading)

            timestamp_bag_nano = int(frame_stamp_secs * 1e9) + int(frame_stamp_nsecs)

            # Collect dynamic transforms:
            transform = TransformStamped()
            transform.header.frame_id = "map"
            timestamp = builtin_interfaces.msg.Time()
            timestamp.sec = frame_stamp_secs
            timestamp.nanosec = frame_stamp_nsecs
            transform.header.stamp = timestamp
            rotation_matrix = np.array([(frame.pose.transform[0], frame.pose.transform[1], frame.pose.transform[2]),
                                        (frame.pose.transform[4], frame.pose.transform[5], frame.pose.transform[6]),
                                        (frame.pose.transform[8], frame.pose.transform[9], frame.pose.transform[10])])
            mat_trans_frame = np.array([(frame.pose.transform[0], frame.pose.transform[1], frame.pose.transform[2],
                                          frame.pose.transform[3]),
                                         (frame.pose.transform[4], frame.pose.transform[5], frame.pose.transform[6],
                                          frame.pose.transform[7]),
                                         (frame.pose.transform[8], frame.pose.transform[9], frame.pose.transform[10],
                                          frame.pose.transform[11]),
                                         (0, 0, 0, 1)])
            # Define T inverse of odom:
            if frame_id == 0:
                self.trans_t_zero_inverse = np.linalg.inv(mat_trans_frame)

            mat_trans_frame = np.dot(self.trans_t_zero_inverse, mat_trans_frame)
            rotation_matrix = mat_trans_frame[0:3, 0:3]

            r, p, y = self.waymoStuff.rotationMatrixToEulerAngles(rotation_matrix)
            q = Quaternion.from_euler(r, p, y, degrees=False)
            coeff = 1
            if (q.z < 0):
                coeff = -1

            transform.transform.rotation.x = q.x * coeff
            transform.transform.rotation.y = q.y * coeff
            transform.transform.rotation.z = q.z * coeff
            transform.transform.rotation.w = q.w * coeff
            transform.transform.translation.x = mat_trans_frame[0][3]
            transform.transform.translation.y = mat_trans_frame[1][3]
            transform.transform.translation.z = mat_trans_frame[2][3]
            transform.child_frame_id = "vehicle"
            transforms.transforms.append(transform)

            # Collect odometry msgs:
            odometry_msg = Odometry()  # Members: 1) PoseWithCovariance 2) TwistWithCovariance
            odometry_msg.header = transform.header
            odometry_msg.child_frame_id = transform.child_frame_id

            pose_and_cov = PoseWithCovariance()  # Members: 1) pose 2) covariance
            pose_and_cov.pose.position.x = transform.transform.translation.x
            pose_and_cov.pose.position.y = transform.transform.translation.y
            pose_and_cov.pose.position.z = transform.transform.translation.z

            pose_and_cov.pose.orientation.x = transform.transform.rotation.x
            pose_and_cov.pose.orientation.y = transform.transform.rotation.y
            pose_and_cov.pose.orientation.z = transform.transform.rotation.z
            pose_and_cov.pose.orientation.w = transform.transform.rotation.w

            np.set_printoptions(precision=3)
            # print("Frame transform:", frame.pose.transform)
            # print("Frame transform:", np.array(frame.pose.transform).reshape((4,4)))
            # print("roll pitch yaw:", r, p, y)
            # print("Odom qx qy qz qw:", quaternion.x, quaternion.y, quaternion.z, quaternion.w)

            odometry_msg.pose = pose_and_cov

            # Collect static transforms:
            camera_info_all_single_frame = []
            for image in frame.images:
                name_frame_id = ''

                if image.name == 1:
                    name_frame_id = 'cam_front'
                if image.name == 2:
                    name_frame_id = 'cam_frontleft'
                if image.name == 3:
                    name_frame_id = 'cam_frontright'
                if image.name == 4:
                    name_frame_id = 'cam_sideleft'
                if image.name == 5:
                    name_frame_id = 'cam_sideright'

                calibration = self.waymoStuff.find_camera_calibration(image.name, frame.context.camera_calibrations)

                transform = TransformStamped()
                transform.header.frame_id = 'vehicle'
                transform.child_frame_id = name_frame_id
                timestamp = builtin_interfaces.msg.Time()
                timestamp.sec = frame_stamp_secs
                timestamp.nanosec = frame_stamp_nsecs
                transform.header.stamp = timestamp

                calibration_array = np.array([(calibration.extrinsic.transform[0], calibration.extrinsic.transform[1],
                                               calibration.extrinsic.transform[2], calibration.extrinsic.transform[3]),
                                              (calibration.extrinsic.transform[4], calibration.extrinsic.transform[5],
                                               calibration.extrinsic.transform[6], calibration.extrinsic.transform[7]),
                                              (calibration.extrinsic.transform[8], calibration.extrinsic.transform[9],
                                               calibration.extrinsic.transform[10],
                                               calibration.extrinsic.transform[11]),
                                              (calibration.extrinsic.transform[12], calibration.extrinsic.transform[13],
                                               calibration.extrinsic.transform[14],
                                               calibration.extrinsic.transform[15])])

                rotation_matrix = np.array([(calibration_array[0][0], calibration_array[0][1], calibration_array[0][2]),
                                            (calibration_array[1][0], calibration_array[1][1], calibration_array[1][2]),
                                            (
                                                calibration_array[2][0], calibration_array[2][1],
                                                calibration_array[2][2])])

                r, p, y = self.waymoStuff.rotationMatrixToEulerAngles(rotation_matrix)
                q = Quaternion.from_euler(r, p, y, degrees=False)
                quaternion = q.normalize
                transform.transform.rotation.x = quaternion.x
                transform.transform.rotation.y = quaternion.y
                transform.transform.rotation.z = quaternion.z
                transform.transform.rotation.w = quaternion.w
                transform.transform.translation.x = calibration_array[0][3]
                transform.transform.translation.y = calibration_array[1][3]
                transform.transform.translation.z = calibration_array[2][3]
                transforms.transforms.append(transform)

                # Intrinsic:
                camera_info = CameraInfo()
                camera_info.header.frame_id = name_frame_id
                timestamp = builtin_interfaces.msg.Time()
                timestamp.sec = frame_stamp_secs
                timestamp.nanosec = frame_stamp_nsecs
                camera_info.header.stamp = timestamp
                camera_info.height = calibration.height
                camera_info.width = calibration.width
                camera_info.k = np.array(calibration.intrinsic).reshape((1, 9)).astype(np.float64).ravel()
                timestamp_bag_nano = int(frame_stamp_secs * 1e9) + int(frame_stamp_nsecs)
                camera_info_all_single_frame.append([camera_info, '/camera_info_' + name_frame_id, timestamp_bag_nano])

            timestamp_bag_nano = int(frame_stamp_secs * 1e9) + int(frame_stamp_nsecs)
            self.list_frame_by_frame_trans.append([transforms, '/tf', timestamp_bag_nano])
            self.list_frame_by_frame_camera_info.append(camera_info_all_single_frame)
            self.list_frame_by_frame_odometry.append([odometry_msg, '/odometry', timestamp_bag_nano])

            # START: ground truth objects as a Autoware.Auto detected objects msgs #########
            detected_objects = DetectedObjects()
            detected_objects.header.frame_id = "vehicle"
            timestamp = builtin_interfaces.msg.Time()
            timestamp.sec = frame_stamp_secs
            timestamp.nanosec = frame_stamp_nsecs
            detected_objects.header.stamp = timestamp
            # Iterate over ground truths in a single frame:
            for index, laser_label in enumerate(frame.laser_labels):
                ID = codecs.encode(laser_label.id[len(laser_label.id) - 8:len(laser_label.id)])
                ID = int.from_bytes(ID, byteorder='big', signed=False)
                ID = int(str(ID)[:5])

                # Mapping Waymo classes to Autoware.Auto classes:
                object_type = -1
                if laser_label.type == 1:  # Vehicle
                    object_type = 1
                elif laser_label.type == 2:  # Pedestrian
                    object_type = 6
                elif laser_label.type == 4:  # Cyclist
                    object_type = 5
                list_types = [1, 2, 4]  # [1, 2, 4]
                # Don't write other classes in Waymo Open Dataset:
                if laser_label.type not in list_types:
                    continue

                # print("Laser label: ", laser_label)
                detected_object = DetectedObject()
                object_classification = ObjectClassification()
                kinematics = DetectedObjectKinematics()
                shape = Shape()

                # Classification:
                object_classification.classification = object_type
                object_classification.probability = 1.0
                detected_object.classification.append(object_classification)
                # Existence Probability:
                detected_object.existence_probability = 1.0
                # Kinematics:
                kinematics.pose.pose.position.x = laser_label.box.center_x
                kinematics.pose.pose.position.y = laser_label.box.center_y
                kinematics.pose.pose.position.z = laser_label.box.center_z
                q = Quaternion.from_euler(0, 0, laser_label.box.heading, degrees=False)
                q = q.normalize
                kinematics.pose.pose.orientation.x = q.x
                kinematics.pose.pose.orientation.y = q.y
                kinematics.pose.pose.orientation.z = q.z
                kinematics.pose.pose.orientation.w = q.w
                kinematics.has_pose = True
                kinematics.has_pose_covariance = False
                kinematics.has_twist = False
                kinematics.has_twist_covariance = False
                detected_object.kinematics = kinematics
                # Shape:
                polygon_point_generator = PolygonPointGenerator(laser_label.box.center_x,
                                                                laser_label.box.center_y,
                                                                laser_label.box.center_z,
                                                                laser_label.box.length,
                                                                laser_label.box.width,
                                                                laser_label.box.heading,
                                                                ID,
                                                                "vehicle",
                                                                frame_stamp_secs,
                                                                frame_stamp_nsecs)
                marker_polygon_points, list_polygon_points = polygon_point_generator.giveMarkerPolygonPointsANDPolygon()

                for point in list_polygon_points:
                    shape.polygon.points.append(point)
                shape.height = laser_label.box.height
                detected_object.shape = shape

                # Insert into detected objectS msg:
                lidar_label_markerArr.markers.append(marker_polygon_points)
                detected_objects.objects.append(detected_object)

            timestamp_bag_nano = int(frame_stamp_secs * 1e9) + int(frame_stamp_nsecs)
            self.list_frame_by_frame_detected_objects.append(
                [detected_objects, '/detected_objects', timestamp_bag_nano])


            self.list_frame_by_frame_lidar_labels.append([lidar_label_markerArr, '/lidar_labels', timestamp_bag_nano])
            # END: #########################################################################
            self.marker_deleter.updateState()
            print(
                "Frame end:#############################################################################################")

            '''if (frame_id == 15):
                print("BREAKED")
                break'''

        return self.list_frame_by_frame_lidar_data, self.list_frame_by_frame_camera_data, \
               self.list_frame_by_frame_lidar_labels, self.list_frame_by_frame_trans, \
               self.list_frame_by_frame_camera_info, self.list_frame_by_frame_detected_objects, \
               self.list_frame_by_frame_odometry, len(list_frame_ids)

    def point_cloud(self, points, parent_frame):
        ros_dtype = sensor_msgs.PointField.FLOAT32
        dtype = np.float32
        itemsize = np.dtype(dtype).itemsize  # A 32-bit float takes 4 bytes.

        data = points.astype(dtype).tobytes()

        fields = [sensor_msgs.PointField(
            name=n, offset=i * itemsize, datatype=ros_dtype, count=1)
            for i, n in enumerate('xyz')]

        header = std_msgs.Header(frame_id=parent_frame)
        return PointCloud2(
            header=header,
            height=1,
            width=points.shape[0],
            is_dense=False,
            is_bigendian=False,
            fields=fields,
            point_step=(itemsize * 3),
            row_step=(itemsize * 3 * points.shape[0]),
            data=data
        )

    def giveIdLabelText(self, frame_stamp_secs, frame_stamp_nsecs, ID, laser_label, frame_id):
        marker = Marker()
        marker.action = Marker.ADD
        marker.header.frame_id = "vehicle"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = frame_stamp_secs
        timestamp.nanosec = frame_stamp_nsecs
        marker.header.stamp = timestamp
        marker.id = ID + frame_id
        marker.ns = "marker_text"
        marker.type = Marker.TEXT_VIEW_FACING
        marker.pose.position.x = laser_label.box.center_x
        marker.pose.position.y = laser_label.box.center_y
        marker.pose.position.z = laser_label.box.center_z + 1
        marker.scale.x = 1.0
        marker.scale.y = 1.0
        marker.scale.z = 1.0
        marker.color.r = 1.0
        marker.color.g = 1.0
        marker.color.b = 1.0
        marker.color.a = 1.0
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = 0.0
        marker.pose.orientation.w = 1.0
        marker.text = str(ID)
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)
        return marker

    def giveCubeLabel(self, frame_stamp_secs, frame_stamp_nsecs, ID, laser_label, frame_id):
        vehicle = (0.0, 0.0, 255.0)
        pedestrian = (0.0, 255.0, 0.0)
        cyclist = (255.0, 0.0, 0.0)
        sign = (0.0, 255.0, 255.0)

        marker = Marker()
        marker.header.frame_id = "vehicle"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = frame_stamp_secs
        timestamp.nanosec = frame_stamp_nsecs
        marker.header.stamp = timestamp
        marker.type = Marker.CUBE
        marker.action = Marker.ADD
        marker.id = ID + frame_id
        marker.ns = "marker_cube"
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)
        marker.pose.position.x = laser_label.box.center_x
        marker.pose.position.y = laser_label.box.center_y
        marker.pose.position.z = laser_label.box.center_z
        q = Quaternion.from_euler(0, 0, laser_label.box.heading, degrees=False)
        q = q.normalize
        marker.pose.orientation.x = q.x  # quaternion[1]
        marker.pose.orientation.y = q.y  # quaternion[2]
        marker.pose.orientation.z = q.z  # quaternion[3]
        marker.pose.orientation.w = q.w  # quaternion[0]
        marker.scale.x = laser_label.box.length
        marker.scale.y = laser_label.box.width
        marker.scale.z = laser_label.box.height
        marker.color.a = 0.5
        if laser_label.type == 1:
            marker.color.b = vehicle[0]
            marker.color.g = vehicle[1]
            marker.color.r = vehicle[2]

        elif laser_label.type == 2:
            marker.color.b = pedestrian[0]
            marker.color.g = pedestrian[1]
            marker.color.r = pedestrian[2]

        elif laser_label.type == 3:
            marker.color.b = sign[0]
            marker.color.g = sign[1]
            marker.color.r = sign[2]

        elif laser_label.type == 4:
            marker.color.b = cyclist[0]
            marker.color.g = cyclist[1]
            marker.color.r = cyclist[2]

        return marker

    def giveArrowMarker(self, frame_stamp_secs, frame_stamp_nsecs, laser_label, ID, frame_id):

        ratio = 3
        marker = Marker()
        marker.color.a = 1.0
        marker.color.r = 0.5
        marker.color.g = 1.0
        marker.color.b = 0.5
        if laser_label.type == 1:
            ratio = 3
            marker.color.a = 1.0
            marker.color.r = 0.0
            marker.color.g = 0.0
            marker.color.b = 1.0
        elif laser_label.type == 2:
            ratio = 1
        elif laser_label.type == 3:
            ratio = 1
        elif laser_label.type == 4:
            ratio = 1

        start_x = laser_label.box.center_x
        start_y = laser_label.box.center_y
        start_z = laser_label.box.center_z

        yaw = laser_label.box.heading

        end_x = start_x + np.cos(yaw) * ratio
        end_y = start_y + np.sin(yaw) * ratio
        end_z = start_z

        marker.header.frame_id = "vehicle"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = frame_stamp_secs
        timestamp.nanosec = frame_stamp_nsecs
        marker.header.stamp = timestamp
        marker.type = Marker.ARROW
        marker.action = Marker.ADD
        marker.id = ID + frame_id
        marker.ns = "marker_arrow"
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)

        marker.scale.x = 0.1
        marker.scale.y = 0.1
        marker.scale.z = 0.4

        p1 = geometry_msgs.msg.Point()
        p2 = geometry_msgs.msg.Point()
        p1.x = start_x
        p1.y = start_y
        p1.z = start_z
        p2.x = end_x
        p2.y = end_y
        p2.z = end_z
        marker.points.append(p1)
        marker.points.append(p2)
        return marker

    def areaConvexPolygon(self, list_polyon_points):
        x_val = []
        y_val = []
        points = []
        for p in list_polyon_points:
            x_val.append(p.x)
            y_val.append(p.y)
            points.append([p.x, p.y])

        plus = x_val[0] * y_val[1] + x_val[1] * y_val[2] + x_val[2] * y_val[3] + x_val[3] * y_val[0]
        minus = y_val[0] * x_val[1] + y_val[1] * x_val[2] + y_val[2] * x_val[3] + y_val[3] * x_val[0]
        area = 0.5 * (plus - minus)
        return area
