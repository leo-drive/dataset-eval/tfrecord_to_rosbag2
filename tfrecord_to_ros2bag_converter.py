import rosbag2_py
from rclpy.serialization import deserialize_message, serialize_message
from rosidl_runtime_py.utilities import get_message
import os
import sys

from os import listdir
from os.path import isfile, join
from Helper import Helper

def get_rosbag_options(path, serialization_format='cdr'):
    storage_options = rosbag2_py.StorageOptions(uri=path, storage_id='sqlite3')
    converter_options = rosbag2_py.ConverterOptions(
        input_serialization_format=serialization_format,
        output_serialization_format=serialization_format)
    return storage_options, converter_options


def create_topic(writer, topic_name, topic_type, serialization_format='cdr'):
    """
    Create a new topic.
    :param writer: writer instance
    :param topic_name:
    :param topic_type:
    :param serialization_format:
    :return:
    """
    topic_name = topic_name
    topic = rosbag2_py.TopicMetadata(name=topic_name, type=topic_type,
                                     serialization_format=serialization_format)
    writer.create_topic(topic)


if __name__ == '__main__':
    path_tf_folder = sys.argv[1]#'/home/goktug/Desktop/Waymo_Valid_Dataset/valid0/validation_validation_0000'
    output_folder_path = sys.argv[2]  # '/home/goktug/ros2bags'

    list_name_tfrecords = [f for f in listdir(path_tf_folder) if isfile(join(path_tf_folder, f))]

    # Iterate over tf paths:
    for tf_name in list_name_tfrecords:
        path_ros2bag = output_folder_path + '/' + tf_name.split('.')[0]
        path_tfrecord = path_tf_folder + '/' + tf_name
        # Read tfrecord:
        helper = Helper(path_tfrecord)
        list_frame_by_frame_lidar_data, \
        list_frame_by_frame_camera_data, \
        list_frame_by_frame_lidar_labels, \
        list_frame_by_frame_trans, \
        list_frame_by_frame_camera_info, \
        list_frame_by_frame_detected_objects, \
        list_frame_by_frame_odometry, frame_count = helper.readDataset()

        storage_options, converter_options = get_rosbag_options(path_ros2bag)
        writer = rosbag2_py.SequentialWriter()
        writer.open(storage_options, converter_options)

        # Iterate over frames:
        for frame_id in range(frame_count):
            single_frame_all_lidar_data = list_frame_by_frame_lidar_data[frame_id]
            single_frame_all_camera_data = list_frame_by_frame_camera_data[frame_id]
            single_frame_lidar_labels = list_frame_by_frame_lidar_labels[frame_id]
            single_frame_trans = list_frame_by_frame_trans[frame_id]
            single_frame_camera_info = list_frame_by_frame_camera_info[frame_id]
            single_frame_detected_objects = list_frame_by_frame_detected_objects[frame_id]
            single_frame_odometry = list_frame_by_frame_odometry[frame_id]

            # 0: msg, 1: topic_name, 2: time_stamp
            # Iterate over lidars:
            for single_frame_single_lidar_data in single_frame_all_lidar_data:
                create_topic(writer, single_frame_single_lidar_data[1], 'sensor_msgs/msg/PointCloud2')
                writer.write(single_frame_single_lidar_data[1],
                             serialize_message(single_frame_single_lidar_data[0]),
                             single_frame_single_lidar_data[2])
            # Iterate over cameras:
            for single_frame_single_camera_data in single_frame_all_camera_data:
                create_topic(writer, single_frame_single_camera_data[1], 'sensor_msgs/msg/Image')
                writer.write(single_frame_single_camera_data[1],
                             serialize_message(single_frame_single_camera_data[0]),
                             single_frame_single_camera_data[2])
            # Write lidar labels:
            create_topic(writer, single_frame_lidar_labels[1], 'visualization_msgs/msg/MarkerArray')
            writer.write(single_frame_lidar_labels[1],
                         serialize_message(single_frame_lidar_labels[0]),
                         single_frame_lidar_labels[2])
            # Write tf msgs:
            create_topic(writer, single_frame_trans[1], 'tf2_msgs/msg/TFMessage')
            writer.write(single_frame_trans[1],
                         serialize_message(single_frame_trans[0]),
                         single_frame_trans[2])
            # Iterate camera info msgs:
            for single_frame_single_camera_info in single_frame_camera_info:
                create_topic(writer, single_frame_single_camera_info[1], 'sensor_msgs/msg/CameraInfo')
                writer.write(single_frame_single_camera_info[1],
                             serialize_message(single_frame_single_camera_info[0]),
                             single_frame_single_camera_info[2])
            # Write ground truth detected objects:
            create_topic(writer, single_frame_detected_objects[1], 'autoware_auto_msgs/msg/DetectedObjects')
            writer.write(single_frame_detected_objects[1],
                         serialize_message(single_frame_detected_objects[0]),
                         single_frame_detected_objects[2])
            # Write odometry in map frame:
            create_topic(writer, single_frame_odometry[1], 'nav_msgs/msg/Odometry')
            writer.write(single_frame_odometry[1],
                         serialize_message(single_frame_odometry[0]),
                         single_frame_odometry[2])

        #break





        print(path_ros2bag, " DONE!")
        print('************************************************************************************************')

    print("All files are extracted.")

    '''helper = Helper(sys.argv[1])
    list_msg_point_cloud, list_time_stamp = helper.readDataset()

    storage_options, converter_options = get_rosbag_options('/home/goktug/rosbag2file')
    writer = rosbag2_py.SequentialWriter()
    writer.open(storage_options, converter_options)
    topic_name = '/cloud_waymo'
    create_topic(writer, topic_name, 'sensor_msgs/msg/PointCloud2')
    for i in range(len(list_msg_point_cloud)):
        time_stamp = int(list_time_stamp[i].sec * 1e9) + int(list_time_stamp[i].nanosec)
        msg_cloud = list_msg_point_cloud[i]
        writer.write(topic_name, serialize_message(msg_cloud), time_stamp)
    print("Extraction is done.")'''

    # Read:
    '''storage_options, converter_options = self.get_rosbag_options('/home/goktug/rosbag2file')
    reader = rosbag2_py.SequentialReader()
    reader.open(storage_options, converter_options)
    topic_types = reader.get_all_topics_and_types()
    type_map = {topic_types[i].name: topic_types[i].type for i in range(len(topic_types))}
    storage_filter = rosbag2_py.StorageFilter(topics=['/cloud_waymo'])
    reader.set_filter(storage_filter)

    while reader.has_next():
        (topic, data, t) = reader.read_next()
        msg_type = get_message(type_map[topic])
        msg = deserialize_message(data, msg_type)
        print(t)'''
